import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapsComponent } from './src/maps.component';

export * from './src/maps.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    MapsComponent
  ],
  exports: [
    MapsComponent,
  ]
})
export class MapsModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MapsModule,
      providers: []
    };
  }
}
