import { Component, OnInit, OnChanges, Input, Output, ViewEncapsulation, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';

import * as L from 'leaflet';
import * as LabeledMarker from 'leaflet-labeled-circle';

@Component({
  selector: 'maps',
  template: '<div id="map"></div>',
  styleUrls: ['./maps.component.css',
    '../node_modules/leaflet/dist/leaflet.css'],
  encapsulation: ViewEncapsulation.None
})
export class MapsComponent implements OnInit, OnChanges {
  @Output() hashtagsSelected = new EventEmitter();
  @Output() clusterSelected = new EventEmitter();
  @Input() topicId: any= '9007';
  @Input() cluster: any;
  @Input() periode: any;
  @Input() typeMap: any;
  @Input() hashtags_state: any;
  @Input() cluster_state: any;
  @Input() mediaId: any;

  private hidePop = 'none';
  private area_id: any;
  private area_name: any;
  private layerProp: any;
  private layerPropGroup: any = [];
  private layerKab: any;
  private layerKabGroup: any = [];
  private map: any;
  private typeLoc: any;
  private mediaName: any;


  map_busy: Subscription;

  constructor(
    private elementRef: ElementRef
  ) { }

  ngOnInit() {
    console.log(LabeledMarker);
    if (this.map === undefined) {
      this.map = L.map('map', {
        zoomControl: false,
        minZoom: 4,
        maxZoom: 19,
        layers: [
          L.tileLayer('http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
            attribution: `&copy; 
                        <a href='http://www.openstreetmap.org/copyright'>OpenStreetMap</a>, 
                        Tiles courtesy of <a href='http://hot.openstreetmap.org/' 
                        target='_blank'>Humanitarian OpenStreetMap Team</a>`
          })]
      });

      L.control.zoom({ position: 'topright' }).addTo(this.map);
      L.control.scale().addTo(this.map);


      let options = {
        'id': 7,
        'country_name': 'malaysia',
        'country_displayname': 'Malaysia',
        'longitude': 109.368164,
        'latitude': 4.455951,
        'scale': 6,
        'region_name': ''
      };
      this.map.setView(L.latLng(options.latitude, options.longitude), options.scale);
    }

    if (this.topicId && this.cluster && this.periode) {
      this.drawMap();
    }
  }

  ngOnChanges() {

  }

  drawMap() {
    let markerPropinsi = [];
    let markerKabupaten = [];
    this.layerPropGroup = [];
    this.layerKabGroup = [];
    this.hidePop = 'none';

    if (this.map.hasLayer(this.layerKab)) {
      this.map.removeLayer(this.layerKab);
    }
    if (this.map.hasLayer(this.layerProp)) {
      this.map.removeLayer(this.layerProp);
    }
    if (this.map_busy !== undefined) {
      this.map_busy.unsubscribe();
    }
    // this.map_busy = this.dataService.getMapLocation(this.topicId, this.cluster, this.periode, 'compare_topic')
    //   .subscribe(data_location => {
    let data_location: any = {
      'data': {
        'topic': {
          'clusterID': 'malaysia'
        },
        'cluster': {
          'id': 'malaysia',
          'name': 'Malaysia',
          'lon': 109.368164,
          'lat': 4.455951,
          'scale': 6
        },
        'propList': [
          {
            'id': 'johor',
            'propinsi': 'Johor',
            'lon': 103.5,
            'lat': 2,
            'stat': {
              '9007': 0
            },
            'stat_sentiment': {
              'positif': 0,
              'neutral': 0,
              'negatif': 0
            },
            'kabupaten': [
              {
                'stat_kb_sentiment': {},
                'name': 'Johor',
                'stat_kb': {},
                'lon': '103.50000000000000',
                'id': 'johor',
                'lat': '2.00000000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Johor Bahru',
                'stat_kb': {},
                'lon': '103.66700000000000',
                'id': 'johor+bahru',
                'lat': '1.63333000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Segamat',
                'stat_kb': {},
                'lon': '102.81700000000000',
                'id': 'segamat',
                'lat': '2.50000000000000'
              }
            ]
          },
          {
            'id': 'kedah',
            'propinsi': 'Kedah',
            'lon': 100.3628,
            'lat': 6.1283,
            'stat': {
              '9007': 0
            },
            'stat_sentiment': {
              'positif': 0,
              'neutral': 0,
              'negatif': 0
            },
            'kabupaten': [
              {
                'stat_kb_sentiment': {},
                'name': 'Alor Setar',
                'stat_kb': {},
                'lon': '100.36700000000000',
                'id': 'alor+setar',
                'lat': '6.11667000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Kedah',
                'stat_kb': {},
                'lon': '100.36280000000000',
                'id': 'kedah',
                'lat': '6.12830000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Kuah',
                'stat_kb': {},
                'lon': '99.73330000000000',
                'id': 'kuah',
                'lat': '6.33333000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Langkawi',
                'stat_kb': {},
                'lon': '99.80000000000000',
                'id': 'langkawi',
                'lat': '6.36667000000000'
              }
            ]
          },
          {
            'id': 'kelantan',
            'propinsi': 'Kelantan',
            'lon': 102,
            'lat': 5.333333,
            'stat': {
              '9007': 0
            },
            'stat_sentiment': {
              'positif': 0,
              'neutral': 0,
              'negatif': 0
            },
            'kabupaten': [
              {
                'stat_kb_sentiment': {},
                'name': 'Kelantan',
                'stat_kb': {},
                'lon': '102.00000000000000',
                'id': 'kelantan',
                'lat': '5.33333300000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Kota Bharu',
                'stat_kb': {},
                'lon': '102.28300000000000',
                'id': 'kota+bharu',
                'lat': '6.16667000000000'
              }
            ]
          },
          {
            'id': 'kuala+lumpur',
            'propinsi': 'Kuala Lumpur',
            'lon': 101.7,
            'lat': 3.116667,
            'stat': {
              '9007': 0
            },
            'stat_sentiment': {
              'positif': 0,
              'neutral': 0,
              'negatif': 0
            },
            'kabupaten': [
              {
                'stat_kb_sentiment': {},
                'name': 'Kuala Lumpur',
                'stat_kb': {},
                'lon': '101.70000000000000',
                'id': 'kuala+lumpur',
                'lat': '3.16667000000000'
              }
            ]
          },
          {
            'id': 'labuan',
            'propinsi': 'Labuan',
            'lon': 115.216667,
            'lat': 5.35,
            'stat': 0,
            'stat_sentiment': 0,
            'kabupaten': ''
          },
          {
            'id': 'malacca',
            'propinsi': 'Malacca',
            'lon': 102.25,
            'lat': 2.25,
            'stat': {
              '9007': 0
            },
            'stat_sentiment': {
              'positif': 0,
              'neutral': 0,
              'negatif': 0
            },
            'kabupaten': [
              {
                'stat_kb_sentiment': {},
                'name': 'Malacca',
                'stat_kb': {},
                'lon': '102.25000000000000',
                'id': 'malacca',
                'lat': '2.25000000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Malacca Town',
                'stat_kb': {},
                'lon': '102.25000000000000',
                'id': 'malacca+town',
                'lat': '2.26667000000000'
              }
            ]
          },
          {
            'id': 'melaka',
            'propinsi': 'Melaka',
            'lon': 102.25,
            'lat': 2.25,
            'stat': {
              '9007': 0
            },
            'stat_sentiment': {
              'positif': 0,
              'neutral': 0,
              'negatif': 0
            },
            'kabupaten': [
              {
                'stat_kb_sentiment': {},
                'name': 'Melaka',
                'stat_kb': {},
                'lon': '102.25000000000000',
                'id': 'melaka',
                'lat': '2.25000000000000'
              }
            ]
          },
          {
            'id': 'negeri+sembilan',
            'propinsi': 'Negeri Sembilan',
            'lon': 102.254791899999,
            'lat': 2.8707497,
            'stat': {
              '9007': 0
            },
            'stat_sentiment': {
              'positif': 0,
              'neutral': 0,
              'negatif': 0
            },
            'kabupaten': [
              {
                'stat_kb_sentiment': {},
                'name': 'Negeri Sembilan',
                'stat_kb': {},
                'lon': '102.25479189999900',
                'id': 'negeri+sembilan',
                'lat': '2.87074970000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Port Dickson',
                'stat_kb': {},
                'lon': '101.80000000000000',
                'id': 'port+dickson',
                'lat': '2.51667000000000'
              }
            ]
          },
          {
            'id': 'pahang',
            'propinsi': 'Pahang',
            'lon': 103.15,
            'lat': 3.5,
            'stat': {
              '9007': 0
            },
            'stat_sentiment': {
              'positif': 0,
              'neutral': 0,
              'negatif': 0
            },
            'kabupaten': [
              {
                'stat_kb_sentiment': {},
                'name': 'Cameron Highlands',
                'stat_kb': {},
                'lon': '101.33300000000000',
                'id': 'cameron+highlands',
                'lat': '4.53333000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Kuantan',
                'stat_kb': {},
                'lon': '103.21700000000000',
                'id': 'kuantan',
                'lat': '3.61667000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Pahang',
                'stat_kb': {},
                'lon': '103.15000000000000',
                'id': 'pahang',
                'lat': '3.50000000000000'
              }
            ]
          },
          {
            'id': 'perak',
            'propinsi': 'Perak',
            'lon': 100.833333,
            'lat': 4,
            'stat': {
              '9007': 0
            },
            'stat_sentiment': {
              'positif': 0,
              'neutral': 0,
              'negatif': 0
            },
            'kabupaten': [
              {
                'stat_kb_sentiment': {},
                'name': 'Ipoh',
                'stat_kb': {},
                'lon': '101.11700000000000',
                'id': 'ipoh',
                'lat': '5.00000000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Perak',
                'stat_kb': {},
                'lon': '100.83333300000000',
                'id': 'perak',
                'lat': '4.00000000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Sitiawan',
                'stat_kb': {},
                'lon': '100.70000000000000',
                'id': 'sitiawan',
                'lat': '4.21667000000000'
              }
            ]
          },
          {
            'id': 'perlis',
            'propinsi': 'Perlis',
            'lon': 100.140968199999,
            'lat': 6.39846,
            'stat': {
              '9007': 0
            },
            'stat_sentiment': {
              'positif': 0,
              'neutral': 0,
              'negatif': 0
            },
            'kabupaten': [
              {
                'stat_kb_sentiment': {},
                'name': 'Kangar',
                'stat_kb': {},
                'lon': '100.20000000000000',
                'id': 'kangar',
                'lat': '6.43330000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Perlis',
                'stat_kb': {},
                'lon': '100.14096819999900',
                'id': 'perlis',
                'lat': '6.39846000000000'
              }
            ]
          },
          {
            'id': 'pulau+pinang',
            'propinsi': 'Pulau Pinang',
            'lon': 100.25,
            'lat': 5.416667,
            'stat': {
              '9007': 0
            },
            'stat_sentiment': {
              'positif': 0,
              'neutral': 0,
              'negatif': 0
            },
            'kabupaten': [
              {
                'stat_kb_sentiment': {},
                'name': 'Pulau Pinang',
                'stat_kb': {},
                'lon': '100.25000000000000',
                'id': 'pulau+pinang',
                'lat': '5.41666700000000'
              }
            ]
          },
          {
            'id': 'putrajaya',
            'propinsi': 'Putrajaya',
            'lon': 101.67979,
            'lat': 2.9074422,
            'stat': {
              '9007': 0
            },
            'stat_sentiment': {
              'positif': 0,
              'neutral': 0,
              'negatif': 0
            },
            'kabupaten': [
              {
                'stat_kb_sentiment': {},
                'name': 'Putrajaya',
                'stat_kb': {},
                'lon': '101.67979000000000',
                'id': 'putrajaya',
                'lat': '2.90744220000000'
              }
            ]
          },
          {
            'id': 'sabah',
            'propinsi': 'Sabah',
            'lon': 117.2781948,
            'lat': 5.338832,
            'stat': {
              '9007': 0
            },
            'stat_sentiment': {
              'positif': 0,
              'neutral': 0,
              'negatif': 0
            },
            'kabupaten': [
              {
                'stat_kb_sentiment': {},
                'name': 'Kota Kinabalu',
                'stat_kb': {},
                'lon': '116.05000000000000',
                'id': 'kota+kinabalu',
                'lat': '5.93333000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Kudat',
                'stat_kb': {},
                'lon': '116.83300000000000',
                'id': 'kudat',
                'lat': '6.91667000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Labuan',
                'stat_kb': {},
                'lon': '115.25000000000000',
                'id': 'labuan',
                'lat': '5.30000000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Lahad Datu',
                'stat_kb': {},
                'lon': '118.31700000000000',
                'id': 'lahad+datu',
                'lat': '5.03333000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Sabah',
                'stat_kb': {},
                'lon': '117.27819480000000',
                'id': 'sabah',
                'lat': '5.33883200000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Sandakan',
                'stat_kb': {},
                'lon': '118.06700000000000',
                'id': 'sandakan',
                'lat': '5.90000000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Tawau',
                'stat_kb': {},
                'lon': '117.88300000000000',
                'id': 'tawau',
                'lat': '4.26667000000000'
              }
            ]
          },
          {
            'id': 'sarawak',
            'propinsi': 'Sarawak',
            'lon': 113,
            'lat': 2.21,
            'stat': {
              '9007': 0
            },
            'stat_sentiment': {
              'positif': 0,
              'neutral': 0,
              'negatif': 0
            },
            'kabupaten': [
              {
                'stat_kb_sentiment': {},
                'name': 'Bintulu',
                'stat_kb': {},
                'lon': '113.33333333333300',
                'id': 'bintulu',
                'lat': '3.00000000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Kuching',
                'stat_kb': {},
                'lon': '110.33333333333300',
                'id': 'kuching',
                'lat': '1.58333333333333'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Miri',
                'stat_kb': {},
                'lon': '113.75000000000000',
                'id': 'miri',
                'lat': '3.66666666666667'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Sarawak',
                'stat_kb': {},
                'lon': '113.50000000000000',
                'id': 'sarawak',
                'lat': '2.50000000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Sibu',
                'stat_kb': {},
                'lon': '112.25000000000000',
                'id': 'sibu',
                'lat': '2.41666666666667'
              }
            ]
          },
          {
            'id': 'selangor',
            'propinsi': 'Selangor',
            'lon': 101.25,
            'lat': 3.333333,
            'stat': {
              '9007': 0
            },
            'stat_sentiment': {
              'positif': 0,
              'neutral': 0,
              'negatif': 0
            },
            'kabupaten': [
              {
                'stat_kb_sentiment': {},
                'name': 'Klang',
                'stat_kb': {},
                'lon': '101.45000000000000',
                'id': 'klang',
                'lat': '3.03333000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Kuala Selangor',
                'stat_kb': {},
                'lon': '101.25000000000000',
                'id': 'kuala+selangor',
                'lat': '3.36667000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Petaling Jaya',
                'stat_kb': {},
                'lon': '101.65000000000000',
                'id': 'petaling+jaya',
                'lat': '3.08333000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Selangor',
                'stat_kb': {},
                'lon': '101.25000000000000',
                'id': 'selangor',
                'lat': '3.33333300000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Shah Alam',
                'stat_kb': {},
                'lon': '101.51700000000000',
                'id': 'shah+alam',
                'lat': '3.06667000000000'
              }
            ]
          },
          {
            'id': 'terengganu',
            'propinsi': 'Terengganu',
            'lon': 102.989614999999,
            'lat': 5.09363419999999,
            'stat': {
              '9007': 0
            },
            'stat_sentiment': {
              'positif': 0,
              'neutral': 0,
              'negatif': 0
            },
            'kabupaten': [
              {
                'stat_kb_sentiment': {},
                'name': 'Kuala Terengganu',
                'stat_kb': {},
                'lon': '103.13300000000000',
                'id': 'kuala+terengganu',
                'lat': '5.33333000000000'
              },
              {
                'stat_kb_sentiment': {},
                'name': 'Terengganu',
                'stat_kb': {},
                'lon': '102.98961499999900',
                'id': 'terengganu',
                'lat': '5.09363419999999'
              }
            ]
          }
        ]
      },
      'status': true,
      'message': 'success'
    };

    let clusterData = data_location['data'].cluster;
    this.map.setView(L.latLng(clusterData.lat, clusterData.lon), clusterData.scale);

    if (data_location['data'].propList.length > 0) {
      let i = 0;
      let propTotal = 0, kabTotal = 0;
      let propValues = [], kabValues = [];
      while (i < data_location['data'].propList.length) {
        let data = data_location['data'].propList[i];
        let featureValue =
          this.generateFeature(data.id, data.propinsi, data.stat[this.topicId], data.lon, data.lat, 'prop');
        if (parseInt(data.stat[this.topicId]) > 0) {
          propTotal += data.stat[this.topicId];
          propValues.push(data.stat[this.topicId]);
          markerPropinsi.push(featureValue);
        }

        let j = 0;
        if (data_location['data'].propList[i].kabupaten.length > 0) {
          while (j < data_location['data'].propList[i].kabupaten.length) {
            let dataKab = data_location['data'].propList[i].kabupaten[j];
            let featureValueKab =
              this.generateFeature(dataKab.id, dataKab.name, dataKab.stat_kb[this.topicId], dataKab.lon, dataKab.lat, 'kab');
            if (parseInt(dataKab.stat_kb[this.topicId]) > 0) {
              kabTotal += dataKab.stat_kb[this.topicId];
              kabValues.push(dataKab.stat_kb[this.topicId]);
              markerKabupaten.push(featureValueKab);
            }
            j++;
          }
        }
        i++;
      }
      this.generateMarker(markerPropinsi, propValues, propTotal, 'prop');
      this.layerProp = L.layerGroup(this.layerPropGroup);
      this.generateMarker(markerKabupaten, kabValues, kabTotal, 'kab');
      this.layerKab = L.layerGroup(this.layerKabGroup);
      this.map.addLayer(this.layerProp);
    }

    let self = this;
    this.map.on('zoomend', function () {
      if (self.map.getZoom() >= 8) {
        if (self.map.hasLayer(self.layerProp)) {
          self.map.removeLayer(self.layerProp);
        }
        if (!self.map.hasLayer(self.layerKab)) {
          self.map.addLayer(self.layerKab);
        }
      }
      if (self.map.getZoom() < 8) {
        if (self.map.hasLayer(self.layerKab)) {
          self.map.removeLayer(self.layerKab);
        }
        if (!self.map.hasLayer(self.layerProp)) {
          self.map.addLayer(self.layerProp);
        }
      }
    });
    // });
  }

  generateFeature(id: any, name: any, count: any, lon: any, lat: any, type: any) {
    return {
      'type': 'Feature',
      'properties': {
        'type': type,
        'area': id,
        'name': name,
        'count': count,
        'text': name + ' (' + count + ')',
        'labelPosition': [
          lon,
          lat
        ]
      },
      'geometry': {
        'type': 'Point',
        'coordinates': [lon, lat]
      }
    };
  }

  generateMarker(features: any, values: any, totalValues: any, type: any) {
    let sortedValue = values.sort(function (a: any, b: any) { return a - b });
    let min = sortedValue[0];
    let max = sortedValue[values.length - 1];
    let mean = totalValues / values.length;

    let option = {
      markerOptions: {
        color: '#fff',
        radius: 20,
        fillColor: '#F0C92B',
        textStyle: {
          color: '#FF2B2B',
          fontSize: 12,
          fontFamily: 'Arial, sans-serif'
        },
        weight: 1,
        shiftY: 6 // to compensate vertical margin, SVG rect is not accurate  
      }
    };

    let i = 0;
    while (i < features.length) {
      option.markerOptions.radius = this.getValueProportionate(features[i].properties.count, max, min, mean);

      let properties = features[i].properties;
      let marker = new LabeledMarker(
        features[i].geometry.coordinates.slice().reverse(),
        features[i], option).on('click', function (e: any) {
          onClick(e.target.feature.properties);
        });

      if (type === 'prop') {
        this.layerPropGroup.push(marker);
      } else if (type === 'kab') {
        this.layerKabGroup.push(marker);
      }

      i++;
    }

    let self = this;
    function onClick(area: any) {
      self.mediaName = '';
      self.area_name = area.name;
      self.area_id = area.area;
      self.typeLoc = area.type;
      self.hidePop = 'block';
    }
  }

  getValueProportionate(value: any, max: any, min: any, mean: any) {
    let size;

    if (value === 0) {
      size = 5;
    } else {
      if (value > (max / 2) && value <= max) {
        size = 30;
      } else if (value > mean && value <= (max / 2)) {
        size = 25;
      } else if (value > (mean / 2) && value <= mean) {
        size = 20;
      } else if (value > min && value <= (mean / 2)) {
        size = 15;
      } else if (value <= min) {
        size = 10;
      } else {
        size = 10;
      }
    }
    return size;
  }

  hashtagsClick() {
    let data = { 'area_id': this.area_id, 'area_name': this.area_name };
    this.hashtagsSelected.emit(data);
  }

  clusterClick() {
    let data = { 'area_id': this.area_id, 'area_name': this.area_name };
    this.clusterSelected.emit(data);
  }

  onClickMedia(data: any) {
    this.mediaId = data.media_id;
    this.mediaName = data.media_name;
  }

}
